# DCEF

Compare users in a GitLab.com group and provided export file.

## First time setup

1. Download appropriate binary from Releases page.
1. Ensure the binary has the correct permissions using: `chmod +x dcef`

Protip: add command to `$PATH`

1. To view which path folders are available: `echo $PATH`
1. Move the dcef folder into one of the path folders e.g. `sudo mv Downloads/dcef /usr/local/bin`

## Usage

Run `dcef --help` for flag information:

```
  -f string
        Export file to be parsed - provide the tar.gz file.
  -g string
        ID number for GitLab.com group to be checked.
  -json
        Output in JSON format - will include full list in output.
  -t string
        GitLab.com Personal Access Token - must be Admin.
```

All flags are required except `--json`.

EXAMPLE: 

```
dcef -t imaginethisisatoken -g 2564024 -f ~/Downloads/2019-04-01_18-26-114_gitlab_export.tar.gz
```

Using the `--json` flag will output the emails in JSON format, including emails that are in both sources as well as the complete list of emails. Can be used with `jq`. 

## Building

`go` version 1.21 is required to compile. Install Go via asdf, Homebrew or [otherwise](https://go.dev/doc/install).

```bash
git clone https://gitlab.com/gitlab-com/support/toolbox/dcef.git
cd dcef
go get
go build
```

You can cross compile for MacOS with:
`GOOS=darwin GOARCH=amd64 go build`
