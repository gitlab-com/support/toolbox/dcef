package main

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"slices"
)

type Member struct {
	User *User `json:"user"`
}

type User struct {
	PublicEmail string `json:"public_email"`
}

func ExportEmails(exportFile string) ([]string, error) {
	var userEmails []string
	file, err := os.Open(exportFile)
	if err != nil {
		return nil, fmt.Errorf("error reading file %s: %s", exportFile, err)
	}
	archive, err := gzip.NewReader(file)
	if err != nil {
		return nil, fmt.Errorf("error opening zip: %s", err)
	}
	tr := tar.NewReader(archive)
	for {
		a, err := tr.Next()
		if err != nil {
			return nil, fmt.Errorf("error with zip: %s", err)
		}
		var mem Member
		if a.Name == "./tree/project/project_members.ndjson" {
			j, err := ioutil.ReadAll(tr)

			if err != nil {
				return nil, fmt.Errorf("error reading project.json file: %s", err)
			}

			//split on new lines and loop through each record
			for _, member := range bytes.Split(j, []byte("\n")) {
				dec := json.NewDecoder(bytes.NewReader(member))
				if erri := dec.Decode(&mem); erri != nil {
					if erri != io.EOF {
						return nil, fmt.Errorf("error unmarshalling json: %s", erri)
					}

				}

				userEmails = append(userEmails, mem.User.PublicEmail)

			}
			break
		}
	}

	userEmails = slices.Compact(userEmails)
	if len(userEmails) <= 0 {
		return nil, fmt.Errorf("there appear to be 0 users in export file")
	}
	return userEmails, nil
}
