package main

import (
	"encoding/json"
	"fmt"
	"golang.org/x/sync/errgroup"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

const (
	baseUrl = "https://gitlab.com/api/v4/"
)

type UserId struct {
	ID int `json:"id"`
}

type IDs []UserId

type UserEmail struct {
	Email string `json:"email"`
}

func httpGet(endpoint, token string) [][]uint8 {
	req, err := http.NewRequest("GET", baseUrl+endpoint, nil)
	if err != nil {
		log.Fatalf("Error reading request: %s", err)
	}
	req.Header.Set("Private-Token", token)
	client := &http.Client{Timeout: time.Second * 60}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Error reading request: %s", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("Status error for %s: %s", endpoint, resp.Status)
	}

	var b [][]uint8
	body, _ := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	b = append(b, body)
	if !strings.Contains(endpoint, "group") {
		return b
	}
	pageCheck := resp.Header.Get("Link")
	if !strings.Contains(pageCheck, "next") {
		return b
	}
	links := strings.Split(pageCheck, ",")
	for _, link := range links {
		if strings.Contains(link, "next") {
			li := strings.Split(link, ">;")[0]
			l := strings.Split(li, "v4/")[1]
			return append(b, httpGet(l, token)...)
			break
		}
	}
	return b
}

func DotcomList(token, gid string) ([]string, error) {
	var g errgroup.Group
	endpoint := "groups/" + gid + "/members/all?per_page=100"
	userEmails := make(chan string)
	collatedEmails := make(chan []string)

	bslice := httpGet(endpoint, token)
	for _, b := range bslice {
		var ids IDs
		if err := json.Unmarshal(b, &ids); err != nil {
			return nil, fmt.Errorf("unable to read user ids: %s", err)
		}
		for _, id := range ids {
			uid := fmt.Sprintf("users/%v", id.ID)
			g.Go(func() error {
				if userObject := httpGet(uid, token); len(userObject) > 1 {
					return fmt.Errorf("error reading user id: %s", id.ID)
				} else {
					var e UserEmail
					if err := json.Unmarshal(userObject[0], &e); err != nil {
						return fmt.Errorf("unable to read user email for %s: %s", id.ID, err)
					}
					userEmails <- e.Email
				}
				return nil
			})
		}
	}
	go func() {
		var emails []string
		for email := range userEmails {
			emails = append(emails, email)
		}
		collatedEmails <- emails
	}()

	err := g.Wait()
	close(userEmails)
	if err != nil {
		return nil, err
	}
	e := <-collatedEmails
	return e, nil
}
