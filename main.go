package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

type Emails struct {
	DotcomEmails []string `json:"dotcom"`
	ExportEmails []string `json:"export_file"`
	Both         []string `json:"both"`
	All          []string `jsonL"all"`
}

var file = flag.String("f", "", "(required) Export file to be parsed - provide the tar.gz file.")
var pat = flag.String("t", "", "(required) GitLab.com Personal Access Token - must be Admin.")
var group = flag.String("g", "", "(required) ID number for GitLab.com group to be checked.")
var j = flag.Bool("json", false, "Output in JSON format - will include full list in output.")

func main() {
	flag.Parse()
	exportFile := *file
	gid := *group
	token := *pat

	if exportFile == "" || gid == "" || token == "" {
		fmt.Fprintf(os.Stderr, "usage of %s\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	dcCh := make(chan []string, 1)
	dcErr := make(chan error, 1)
	go func() {
		dc, err := DotcomList(token, gid)
		dcCh <- dc
		dcErr <- err
	}()

	efCh := make(chan []string, 1)
	efErr := make(chan error, 1)
	go func() {
		ef, err := ExportEmails(exportFile)
		efCh <- ef
		efErr <- err
	}()

	dcErrOut := <-dcErr
	efErrOut := <-efErr

	switch {
	case dcErrOut != nil:
		fmt.Println(dcErrOut)
		fallthrough
	case efErrOut != nil:
		fmt.Println(efErrOut)
		fallthrough
	case dcErrOut != nil || efErrOut != nil:
		os.Exit(1)
	}

	emails := compareEmails(<-dcCh, <-efCh)

	if len(emails.All) <= 0 {
		log.Fatal("No users found in either GitLab.com group or export file")
	}

	jj := *j
	if !jj {
		fmt.Println("User emails unique to GitLab.com group:")
		fmt.Println("---------------------------------------")
		if len(emails.DotcomEmails) == 0 {
			fmt.Println("NONE")
		}
		for _, d := range emails.DotcomEmails {
			fmt.Println(d)
		}

		fmt.Println("\nUser emails unique to export file:")
		fmt.Println("---------------------------------------")
		if len(emails.ExportEmails) == 0 {
			fmt.Println("NONE")
		}
		for _, e := range emails.ExportEmails {
			fmt.Println(e)
		}
	} else {
		data, _ := json.MarshalIndent(emails, "", "    ")
		fmt.Printf("%s\n", data)
	}
}

func compareEmails(dc, ex []string) Emails {
	umap := make(map[string]int)

	for _, dce := range dc {
		umap[dce]++
	}

	for _, exe := range ex {
		umap[exe] += 2
	}
	var emails Emails
	for k, v := range umap {
		emails.All = append(emails.All, k)
		switch v {
		case 1:
			emails.DotcomEmails = append(emails.DotcomEmails, k)
		case 2:
			emails.ExportEmails = append(emails.ExportEmails, k)
		case 3:
			emails.Both = append(emails.Both, k)
		}
	}
	return emails
}
